package apis

import (
	"fmt"
	"go/entities"
	"go/models"
	"log"
	"net/http"

	"github.com/gorilla/mux"
	"github.com/jinzhu/gorm"
)

type App struct {
	Router *mux.Router
	DB     *gorm.DB
}

func (a *App) Initialize(config *Config) {
	dbURI := fmt.Sprintf("%s:%s@(%s)/%s?charset=%s&parseTime=True",
		config.DB.Username,
		config.DB.Password,
		config.DB.Host,
		config.DB.Name,
		config.DB.Charset)

	db, err := gorm.Open(config.DB.Dialect, dbURI)
	if err != nil {
		log.Fatal("Could not connect database" + err.Error())
	}

	a.DB = entities.DBMigrate(db)
	a.Router = mux.NewRouter()
	a.setRouters()
}

func (a *App) setRouters() {
	// Routing for handling the projects
	a.Get("/user", a.GetAllUsers)
	a.Post("/user", a.CreateUser)
	a.Put("/user/{id}", a.UpdateUser)
	a.Delete("/user/{id}", a.DeleteUser)
}

func (a *App) Get(path string, f func(response http.ResponseWriter, request *http.Request)) {
	a.Router.HandleFunc(path, f).Methods("GET", "OPTIONS")
}

// Wrap the router for POST method
func (a *App) Post(path string, f func(response http.ResponseWriter, request *http.Request)) {
	a.Router.HandleFunc(path, f).Methods("POST", "OPTIONS")
}

// Wrap the router for PUT method
func (a *App) Put(path string, f func(response http.ResponseWriter, request *http.Request)) {
	a.Router.HandleFunc(path, f).Methods("PUT", "OPTIONS")
}

// Wrap the router for DELETE method
func (a *App) Delete(path string, f func(response http.ResponseWriter, request *http.Request)) {
	a.Router.HandleFunc(path, f).Methods("DELETE", "OPTIONS")
}

// Handlers to manage User Data
func (a *App) GetAllUsers(response http.ResponseWriter, request *http.Request) {
	models.GetAllUsers(a.DB, response, request)
}

func (a *App) CreateUser(response http.ResponseWriter, request *http.Request) {
	models.CreateUser(a.DB, response, request)
}

func (a *App) UpdateUser(response http.ResponseWriter, request *http.Request) {
	models.UpdateUser(a.DB, response, request)
}

func (a *App) DeleteUser(response http.ResponseWriter, request *http.Request) {
	models.DeleteUser(a.DB, response, request)
}

// Run the app on it's router
func (a *App) Run(host string) {
	log.Fatal(http.ListenAndServe(host, a.Router))
}
