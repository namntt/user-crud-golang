package main

import (
	"go/apis"
)

func main() {
	config := apis.GetConfig()

	app := &apis.App{}
	app.Initialize(config)
	app.Run(":5000")
}
