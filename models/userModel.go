package models

import (
	"encoding/json"
	"go/entities"
	"net/http"

	"github.com/gorilla/mux"
	"github.com/jinzhu/gorm"
)

func FindUserById(db *gorm.DB, id string, response http.ResponseWriter, request *http.Request) *entities.User {
	enableCors(&response)
	user := entities.User{}
	err := db.First(&user, entities.User{Id: id}).Error
	if err != nil {
		responseWithError(response, http.StatusBadRequest, err.Error())
		return nil
	}
	return &user
}

func GetAllUsers(db *gorm.DB, response http.ResponseWriter, request *http.Request) {
	enableCors(&response)
	users := []entities.User{}
	db.Find(&users)
	responseWithJSON(response, http.StatusOK, users)
}

func CreateUser(db *gorm.DB, response http.ResponseWriter, request *http.Request) {
	enableCors(&response)
	user := entities.User{}

	err := json.NewDecoder(request.Body).Decode(&user)
	if err != nil {
		responseWithError(response, http.StatusBadRequest, err.Error())
	} else {
		result := db.Save(&user)
		if result.Error != nil {
			responseWithError(response, http.StatusBadRequest, "Could not create user"+result.Error.Error())
			return
		}
		responseWithJSON(response, http.StatusOK, user)
	}
}

func UpdateUser(db *gorm.DB, response http.ResponseWriter, request *http.Request) {
	enableCors(&response)
	vars := mux.Vars(request)
	id := vars["ID"]
	user := FindUserById(db, id, response, request)
	if user == nil {
		return
	}

	err := json.NewDecoder(request.Body).Decode(&user)
	if err != nil {
		responseWithError(response, http.StatusBadRequest, err.Error())
	} else {
		result := db.Save(&user)
		if result.Error != nil {
			responseWithError(response, http.StatusBadRequest, "Could not update user"+result.Error.Error())
			return
		}
		responseWithJSON(response, http.StatusOK, "User updated successfully")
	}
}

func DeleteUser(db *gorm.DB, response http.ResponseWriter, request *http.Request) {
	enableCors(&response)
	vars := mux.Vars(request)
	id := vars["ID"]
	user := FindUserById(db, id, response, request)
	if user == nil {
		return
	}

	result := db.Delete(&user)
	if result.Error != nil {
		responseWithError(response, http.StatusBadRequest, "Could not delete  user"+result.Error.Error())
		return
	}
	responseWithJSON(response, http.StatusOK, "User deleted successfully")
}

func responseWithError(response http.ResponseWriter, statusCode int, msg string) {
	responseWithJSON(response, statusCode, map[string]string{
		"error": msg,
	})
}

func responseWithJSON(response http.ResponseWriter, statusCode int, data interface{}) {
	result, _ := json.Marshal(data)
	response.Header().Set("Content-Type", "application/json")
	response.WriteHeader(statusCode)
	response.Write(result)
}

func enableCors(response *http.ResponseWriter) {
	(*response).Header().Set("Access-Control-Allow-Origin", "*")
	(*response).Header().Set("Access-Control-Allow-Headers", "Content-Type")
}
